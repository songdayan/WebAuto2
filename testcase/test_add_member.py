import pytest

from page.main_page import MainPage


class TestAddMember:
    @pytest.mark.parametrize("name",["吴奇隆"])
    @pytest.mark.parametrize("account", ["kkk"])
    @pytest.mark.parametrize("phonum", ["13888888887"])
    def test_add_member(self,name,account,phonum):
        """
        用来测试添加成员功能
        :return:
        """
        #实例化
        self.main = MainPage()
        #链式调用  跳转添加成员    添加成员   获取成员列表做断言验证
        res=self.main.goto_add_member().add_memeber(name,account,phonum).get_list()
        assert name in res


