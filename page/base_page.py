import time

from selenium import webdriver


class BasePage:
    """

    封装通用方法，方便实例化
    """
    def __init__(self,base_driver = None):
        """

        :param base_driver:传入driver实例对象

        """
        if base_driver is not None:
            self.driver = base_driver
        else:
            #使用浏览器复用模式
            fire_arg = webdriver.ChromeOptions()
            # 加入调试地址
            fire_arg.debugger_address = '127.0.0.1:9222'
            # 实例化driver对象
            self.driver = webdriver.Chrome(options=fire_arg)
            self.driver.get("https://work.weixin.qq.com/wework_admin/frame#index")
            self.driver.implicitly_wait(5)

    def find(self,locator):
        #解元组
        return self.driver.find_element(*locator)