import time

from selenium import webdriver
from selenium.webdriver.common.by import By

from page.add_member_page import AddMemberPage
from page.base_page import BasePage
from page.contact_page import ContactPage


class MainPage(BasePage):
    add_member_ele = (By.CSS_SELECTOR,".ww_indexImg_AddMember")
    def goto_add_member(self):
        self.find(self.add_member_ele).click()
        #self.driver.find_element(By.CSS_SELECTOR, ".ww_indexImg_AddMember").click()
                            #避免重复实例化，所有吧driver传进来
        return AddMemberPage(self.driver)
    #contact_button= (By.XPATH,'//*[@class="frame_head_bottom_stage"]/nav/a[2]')
    #点击首页的导入通讯录
    contact_button = (By.XPATH, '//*[@class="index_service"]/div[2]/a[2]/div')
    def goto_contact(self):
        self.find(self.contact_button).click()
        return ContactPage(self.driver)
