from selenium import webdriver
from selenium.webdriver.common.by import By

from page.add_develope_page import AddDevelopePage
from page.base_page import BasePage
from page.contact_page import ContactPage


class AddMemberPage(BasePage):
    def add_memeber(self, name, account, phonum):

        #self.driver.find_element(By.CSS_SELECTOR, ".ww_indexImg_AddMember").click()
        #添加姓名，账号，手机号
        self.driver.find_element(By.ID,"username").send_keys(name)
        self.driver.find_element(By.ID,"memberAdd_acctid").send_keys(account)
        self.driver.find_element(By.ID,"memberAdd_phone").send_keys(phonum)
        #点击保存
        self.driver.find_element(By.CSS_SELECTOR,".qui_btn.ww_btn.js_btn_save").click()

        return ContactPage(self.driver)
    def add_dev(self):
        self.driver.find_element(By.CSS_SELECTOR, ".member_colLeft_top_addBtnWrap.js_create_dropdown").click()
        self.driver.implicitly_wait(5)
        return AddDevelopePage(self.driver)

    def goto_contact(self):
        pass

