from selenium.webdriver.common.by import By

from page.base_page import BasePage
from page.contact_page import ContactPage


class AddDevelopePage(BasePage):
    def adddevelope(self):
        #点击添加部门
        self.driver.find_element(By.CSS_SELECTOR, ".js_create_party").click()
        self.driver.implicitly_wait(2)
        #点击所属部门
        self.driver.find_element(By.CSS_SELECTOR, ".qui_btn.ww_btn.ww_btn_Dropdown.js_toggle_party_list").click()
        self.driver.implicitly_wait(2)
        self.driver.find_element(By.CSS_SELECTOR, ".qui_dialog_body.ww_dialog_body [id='1688850900998192_anchor']").click()
        self.driver.find_element(By.XPATH, '//*[@id="__dialog__MNDialog__"]/div/div[2]/div/form/div[1]/input').send_keys("保安一部")
        self.driver.find_element(By.XPATH,'//*[@id="__dialog__MNDialog__"]/div/div[3]/a[1]').click()
        return ContactPage(self.driver)
